const hola = { name: 'Juan' };
const hola2 = { surname: 'Perez'};
const hola3 = { age: 23, name: "pablo" };

console.log(Object.assign({}, hola, hola2, hola3));

// const hola = {
//   name: 'Juan',
//   ...hola2,
//   ...hola3,
// };
// console.log(hola);

const array2 = [ 'hola' ];
const array1 = [
  'hola1',
  ...array2,
];
console.log(array1);
