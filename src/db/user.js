import jwt from 'jsonwebtoken';

import shortid from 'shortid';
import db from './db';

// const SECRET = process.env.SECRET;
const SECRET = 'password';
const USERS = 'users';

function getUser(id) {
  return db.get(USERS)
    .find({ id })
    .value();
}

function addUser(user) {
  const id = shortid.generate();
  const userSave = Object.assign({}, user, { id });
  db.get(USERS)
    .push(userSave)
    .write();
  return {
    token: jwt.sign({ id }, SECRET),
  };
}

export {
  getUser,
  addUser,
};
