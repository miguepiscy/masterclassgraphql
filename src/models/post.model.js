import { getUser } from '../db/user';
import { getPost, addPost, getPostsOfUser } from '../db/post';

const typePost = `
  type Post {
    id: ID!
    title: String!
    text: String
    avatar: String
    author: User!
  }
`;
const queryPost = `
  post(id: ID!): Post
  myposts: [Post]!
`;
const mutationPost = `
  createPost(title: String!, text: String, avatar: String, author: ID!): Post!
`;

const resolveQueryPost = {
  post(_, { id }) {
    return getPost(id);
  },
  myposts(_, obj, { getAuthUser }) {
    const user = getAuthUser();
    return getPostsOfUser(user.id);
  },
};

const resolveMutationPost = {
  createPost(_, post) {
    const user = getUser(post.author);
    if (!user) {
      throw new Error('Author no exists');
    }
    return addPost(post);
  },
};

const resolveAddedPost = {
  Post: {
    author(parent) {
      return getUser(parent.author);
    },
  },
};

export {
  typePost,
  queryPost,
  mutationPost,
  resolveQueryPost,
  resolveMutationPost,
  resolveAddedPost,
};
